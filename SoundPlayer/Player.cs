﻿using System;
using System.Collections.Generic;
using System.Text;
using Global;
using IrrKlang;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Threading;

namespace SoundPlayer
{
    public enum HowToPlay 
    { 
        RANDOM,
        SEQUENCED,
        DONOT
    }
    public class Player : BasePadCommand, ISoundStopEventReceiver
    {

        ISoundEngine engine = null;
        ISound sound = null;
        HowToPlay how_to_play = HowToPlay.SEQUENCED;
        //string current_playlist = null;
        private int seed = Environment.TickCount;

        private const string supported_files = "mp3|ogg|wav|mod|it|xm|s3d";
        
        [Serializable]
        public class SongItem
        {
            public string Name = String.Empty;
            public string Path = String.Empty;
            public string PlayList = String.Empty;
            public ID3v1 Tag = null;
        }
        
        List<SongItem> songs = new List<SongItem>();
        string current_playlist = null;
        int _song_possition;

        private int song_possition
        {
            get {
                
                return _song_possition; 
            }
            set {
                if (current_playlist == null)
                {
                    if (songs.Count - 1 < value)
                        song_possition = 0;
                    else
                        _song_possition = value;
                }
                else
                {
                    if (songs.FindAll(x => ConsoleEx.CompareWildcard(x.PlayList, current_playlist, true)).Count - 1 < value)
                        song_possition = 0;
                    else
                        _song_possition = value;
                }
            }
        }

        public Player()
        {
            engine = new ISoundEngine();
        }
        
        
        public void PlaySound(string filename_mask, string list_name)
        {
            if (sound != null && sound.Paused) 
            {
                PauseSound();
                return;
            }
            if (String.IsNullOrEmpty(filename_mask))
            {
                if (songs.Count == 0)
                {
                    padControl.InsertText("Specify filename or item in playlists\n");
                    return;
                }
                filename_mask = "*";
            }
            else
                filename_mask = filename_mask.Replace("\"", "");
            SongItem si = null;
            //because name contains author
            string search_in_list_mask = !filename_mask.StartsWith("*") ? String.Format("*{0}", filename_mask) : filename_mask;
            if (list_name == null)
            {
                si = songs.Find(x => ConsoleEx.CompareWildcard(x.Name, search_in_list_mask, true)
                         || ConsoleEx.CompareWildcard(x.Path, search_in_list_mask, true));
            }
            else 
            {
                si = songs.Find(x => (ConsoleEx.CompareWildcard(x.Name, search_in_list_mask, true)
                         || ConsoleEx.CompareWildcard(x.Path, search_in_list_mask, true)) && ConsoleEx.CompareWildcard(x.PlayList, list_name, true));
            
            }
            if (si != null)
            {
                filename_mask = si.Path;
            }
            if (!filename_mask.Contains(":\\"))
                filename_mask = String.Format("{0}\\{1}", GlobalData.CurrentDirectory, filename_mask);

            if (!File.Exists(filename_mask))
            {
                string mask = filename_mask;
                string founded = null;

                foreach (string file in Directory.GetFiles(GlobalData.CurrentDirectory))
                    if (ConsoleEx.CompareWildcard(file, mask, true) == true)
                    {
                        founded = file;
                        break;
                    }
                if (founded == null)
                {
                    padControl.InsertText(String.Format("File doesn't exists {0}", filename_mask));
                    return;
                }
                filename_mask = founded;
            }

            if (si != null)
            {
                int ind = songs.IndexOf(si);
                if (ind != -1)
                    song_possition = ind;
            }
            PlaySoundFromFile(filename_mask);
        }

        AutoResetEvent _aevent = new AutoResetEvent(false);
        private void PlaySoundFromFile(string filename)
        {
            
            StopSound();
            try
            {
               
                sound = engine.Play2D(filename,false,false,StreamMode.AutoDetect,true);
                sound.setSoundStopEventReceiver(this);

            }
            catch (Exception)
            {
                padControl.InsertText(String.Format("Error playing file {0}", filename));
            }
         
        }
        
        public void StopSound() 
        {

            
            if (sound != null)
            {

                //if(!sound.Finished)
                    sound.Stop();
            }
            //engine.StopAllSounds();
        }

        public void PauseSound()
        {
            if (sound == null)
            {
                padControl.InsertText("Sound is not loaded\n");
                return;
            }
            sound.Paused = !sound.Paused;
        }

        public void PlayNext() 
        {
            if (songs.Count == 0 || sound == null)
            {
                padControl.InsertText("Song list is clear");
            }
            song_possition++;
          

            PlaySoundFromFile(songs[song_possition].Path);
        }

        public void PlayPrev()
        {
            if (songs.Count == 0 || sound == null)
            {
                padControl.InsertText("Song list is clear");
            }

            song_possition--;
            PlaySoundFromFile(songs[song_possition].Path);
        }

        public void InvokeEffect(string effect_name)
        {
            if (sound == null)
            {
                padControl.InsertText("Sound is not loaded\n");
                return;
            }
            if (sound.SoundEffectControl == null)
            {
                padControl.InsertText("Sound effects are not available\n");
                return;
            }
            
            Type t = typeof(ISoundEffectControl);
           
            if (!String.IsNullOrEmpty(effect_name))
            {
                try
                {
                    MethodInfo mi = t.GetMethod(effect_name,new Type[]{});//no params
                    mi.Invoke(sound.SoundEffectControl, null);
                }
                catch 
                {
                    padControl.InsertText("Invalid during execution\n");
                }
            }
            else 
            {
                padControl.InsertText("\nAvailable effect functions:\n");
                foreach(MethodInfo mi in t.GetMethods())
                {
                    if (mi.Attributes == MethodAttributes.Public && mi.GetParameters().Length == 0) 
                    {
                        padControl.InsertText(String.Format("\t{0}\n", mi.Name));
                    }
                }
            }
            padControl.InsertText("done\n");
        }

        public void Speed(string speed)
        {
            if (!PadAssertStringNullOrEmpty(speed, "Specify speed value"))
                return;
            if (sound == null)
            {
                padControl.InsertText("Sound is not loaded\n");
                return;
            }
            float _seed = 0.0f;
            if(!float.TryParse(speed,out _seed))
            {
                padControl.InsertText("Invalid speed value\n");
                return;
            }
            sound.PlaybackSpeed = _seed;
        }

        public void Volume(string vol) 
        { 
            if (!PadAssertStringNullOrEmpty(vol, "Specify volume value [0..1]"))
                return;
            if (sound == null)
            {
                padControl.InsertText("Sound is not loaded\n");
                return;
            }
            float _seed = 0.0f;
            if(!float.TryParse(vol,out _seed))
            {
                padControl.InsertText("Invalid volume value\n");
                return;
            }

            sound.Volume = _seed > 1 || _seed < 0 ? 1 : _seed; 
        }

        public void OnSoundStopped(ISound sound, StopEventCause reason, object userData) 
        {
          
            if (reason == StopEventCause.SoundFinishedPlaying)
            {
                if (songs.Count == 0)
                    return;
                switch (how_to_play)
                {
                    case HowToPlay.RANDOM:
                        Random r = new Random(seed++);
                        song_possition = r.Next(songs.Count);
                        break;
                    case HowToPlay.SEQUENCED:
                        song_possition++;
                        break;
                    case HowToPlay.DONOT:
                        _aevent.Set();
                        return;
                }
                PlaySoundFromFile(songs[song_possition].Path);
            }
        }

        #region Play List Operation

        public void RemoveItems(string item,string playlist) 
        {
            if (String.IsNullOrEmpty(item)) 
            {
                padControl.InsertText("Specify item to remove songs.\n");
                return;
            }
            if(String.IsNullOrEmpty(playlist))
                songs.RemoveAll(x => ConsoleEx.CompareWildcard(x.Name, item, true) == true);
            else
                songs.RemoveAll(x => ConsoleEx.CompareWildcard(x.Name, item, true) == true && ConsoleEx.CompareWildcard(x.PlayList, playlist, true) == true);
            padControl.InsertText("Done.\n");
        }

        public void ShowPlayList(string playlist) 
        {

            if (playlist == null) 
            {
                foreach (SongItem song in songs) 
                {

                    ShowSong(song);
                } 
            }else
            {
                foreach (SongItem song in songs.FindAll(x => String.Compare(x.PlayList,playlist,true) == 0))
                {
                    ShowSong(song);
                } 
            }
            padControl.InsertText("done\n");    
        }

        private void ShowSong(SongItem song)
        {
            if (song.Tag == null)
                padControl.InsertText(String.Format("\t{0} => {1}\n", song.PlayList, song.Name));
            else
                padControl.InsertText(String.Format("\t{0} => {1} - {2} - {3}\n", song.PlayList, song.Tag.Artist,
                    song.Tag.Album, song.Tag.Title));
        }

        public void AddItems(string list_name, string mask, string path) 
        {
            if (!PadAssertStringNullOrEmpty(list_name, "Specify playlist name\n"))
                return;
           if(String.IsNullOrEmpty(mask))
           {
                mask = "*";
           }
            if (path == null)
            {
                path = GlobalData.CurrentDirectory;
               
            }
            else
            {
                if (!path.Contains(":\\"))
                    path = String.Format("{0}\\{1}", GlobalData.CurrentDirectory, path);
            }
            //mask = String.Format("{0}\\{1}", path, mask);
            int count = 0;
            if(!Directory.Exists(path))
            {
                padControl.InsertText(String.Format("Directory {0} is not exits",path));
                return;
            }
          //  count += AddItemsFromDirectory(list_name, mask, path);
            
            AddItemsThroughAllDirs(list_name, mask, path, ref count);
            songs.Sort(new SongComparerByPlayList());
            padControl.InsertText(String.Format("{0} items added\n",count));
        }

        public void AddItemsThroughAllDirs(string list_name, string mask, string path, ref int count) 
        {
            count += AddItemsFromDirectory(list_name, mask, path);
            foreach (string dir in Directory.GetDirectories(path))
            {
                AddItemsThroughAllDirs(list_name, mask, dir, ref count);
            }
        }
       
        private int AddItemsFromDirectory(string list_name, string mask, string path)
        {
            int count = 0;
            string[] supported_files_lst = supported_files.Split('|');

            foreach (string file in Directory.GetFiles(path))
            {

                if (!ConsoleEx.CompareWildcard(file, mask, true))
                    continue;

                bool bIsExtenseOk = false;
                foreach (string extens in supported_files_lst)
                {
                    if (ConsoleEx.CompareWildcard(file, String.Format("*.{0}",extens), true))
                    {
                        bIsExtenseOk = true;
                        break;
                    }
                }

                if (bIsExtenseOk)
                {
                    count++;
                    AddSongItem(list_name, file);
                }
            }
            return count;
        }

        private void AddSongItem(string list_name, string file)
        {
            SongItem si = new SongItem();
            si.PlayList = list_name;
            si.Tag = GetID3v1Tag(file);
            //check
            if (si.Tag == null)
                si.Name = file.Substring(file.LastIndexOf('\\') + 1);
            else
            {
               
                ReplaceZeros( ref si.Tag.Artist);
                ReplaceZeros(ref si.Tag.Album);
                ReplaceZeros(ref si.Tag.Comment);
                ReplaceZeros(ref si.Tag.Title);
                ReplaceZeros(ref si.Tag.Year);

                si.Name = String.Format("{0} - {1}", si.Tag.Artist.Replace('\0', ' ').Trim(), si.Tag.Title.Replace('\0', ' ').Trim());
            }
            si.Path = file;
            songs.Add(si);
        }

        private void ReplaceZeros(ref string item)
        {
           // si.Tag.Title = si.Tag.Title.Replace('\0', ' ').Trim();
            item = item.Replace('\0', ' ').Trim();
        }

        private ID3v1 GetID3v1Tag(string file)
        {
            ID3v1 tag = new ID3v1();
            bool fOk = false;
            try
            {
                tag.Read(file);
                if (tag.hasTag && (tag.Artist != String.Empty ||
                    tag.Album != String.Empty ||
                    tag.Title != String.Empty))
                    fOk = true;
            }
            catch
            {
                //tag not found
            }
            if (fOk)
                return tag;
            else
                return null;
        }
        
        public void Current() 
        {
            if (songs.Count == 0 || sound == null)
            {
                padControl.InsertText("Song list is clear");
                return;
            }
            ShowSong(songs[song_possition]);   
        }

        public void GetTagInfo()
        {
            if (songs.Count == 0 || sound == null)
            {
                padControl.InsertText("Song list is clear\n");
                return;
            }

            if (songs[song_possition].Tag != null)
            {
                padControl.InsertText(String.Format("\tArtist: {0}\n\tAlbum: {1}\n\tSong: {2}\n\tComment:{3}\n",
                    songs[song_possition].Tag.Artist, songs[song_possition].Tag.Album, songs[song_possition].Tag.Title,
                    songs[song_possition].Tag.Comment));
            }
            else 
            {
                ShowSong(songs[song_possition]);
                padControl.InsertText("Tag is not exits\n");
            }
        }
        
        public void RemovePlayList(string name) 
        { 
            if(!PadAssertStringNullOrEmpty(name,"Specify playlist name or mask\n"))
                return;
            songs.RemoveAll(x => ConsoleEx.CompareWildcard(x.PlayList, name, true));
            padControl.InsertText("done\n");
        }

        public void SavePlayLists(string filename) 
        {
            if (String.IsNullOrEmpty(filename))
                filename = String.Format("{0}\\playlists.txt",GlobalData.CurrentDirectory);
            try
            {
                using (StreamWriter sw = new StreamWriter(filename, false))
                {
                    foreach (SongItem song in songs) 
                    {
                        sw.WriteLine(String.Format("{0}|{1}", song.PlayList, song.Path));
                    }
                    sw.Close();
                }
            }
            catch 
            {
                padControl.InsertText("Error save playlists\n");
                return;
            }
            padControl.InsertText("done\n");
        }

        public void LoadPlayLists(string filename) 
        {
            if (String.IsNullOrEmpty(filename))
                filename = String.Format("{0}\\playlists.txt", GlobalData.CurrentDirectory);
            try
            {
                songs.Clear();
                using (StreamReader sr = new StreamReader(filename))
                {
                    while (sr.Peek() >= 0) 
                    {
                        string [] splitted = sr.ReadLine().Split('|');
                        if (splitted.Length < 2)
                            continue;
                        AddSongItem(splitted[0], splitted[1]);
                    }
                }
            }
            catch
            {
                padControl.InsertText("Error load playlists\n");
                return;
            }
            padControl.InsertText("done\n");
        }

        public class SongComparerByPlayList : IComparer<SongItem> 
        {
            public int Compare(SongItem si, SongItem si2) 
            {
                int compare = String.Compare(si.PlayList, si2.PlayList, true);
                if (compare == 0) 
                { 
                    if(si.Tag != null && si2.Tag != null){
                        compare = String.Compare(si.Tag.Artist, si2.Tag.Artist, true);
                        if (compare == 0) 
                        {
                            compare = String.Compare(si.Tag.Album + si.Tag.Title, si2.Tag.Album + si2.Tag.Title, true);
                        }
                    }else
                        if (si.Tag == null && si2.Tag == null) 
                        {
                            compare = String.Compare(si.Name, si2.Name, true);
                        }
                }
                return compare;
            }
        } 
        #endregion

        #region Play Loop
        public void PlayInfo()
        {
            string playl = current_playlist;
            if (playl == null)
                playl = "all playlist";
            padControl.InsertText(String.Format("\t Looped playlist: {0}\n", playl));
            padControl.InsertText(String.Format("\t Play cycl type: {0}\n", how_to_play.ToString()));
        }

        public void HowPlay(string how_to_play, string play_list)
        {
            if (String.IsNullOrEmpty(how_to_play))
            {
                PlayInfo();
                return;
            }
            current_playlist = null;
            if (play_list != null)
                current_playlist = play_list;
            int howt = 0;
            if (!int.TryParse(how_to_play, out howt))
            {
                padControl.InsertText("Invalid how_to_play param\n");
                return;
            }
            switch (howt)
            {
                case 0:
                    this.how_to_play = HowToPlay.DONOT;
                    break;
                case 1:
                    this.how_to_play = HowToPlay.SEQUENCED;
                    break;
                case 2:
                    this.how_to_play = HowToPlay.RANDOM;
                    break;
                default:
                    padControl.InsertText("Invalid how_to_play param (use: 0 - stop any, do not play,1 - play consistent,2- play randomly)\n");
                    return;
            }
            PlayInfo();
            padControl.InsertText("done\n");
        }
        #endregion
    }
}
