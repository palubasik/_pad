using System.Windows.Forms;
using PadUserControls;
namespace Pad
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.richTextBoxPad = new PadUserControls.RichTextBoxPad();
            this.notifyIconCopyPast = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemAutoExec = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemShow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.timerInternetConnectionCheck = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerConnectionCheck = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStripTray.SuspendLayout();
            this.SuspendLayout();
           
            // 
            // richTextBoxPad
            // 
            this.richTextBoxPad.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.richTextBoxPad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxPad.ForeColor = System.Drawing.SystemColors.WindowText;
            this.richTextBoxPad.HiglightColor = PadUserControls.RtfColor.White;
            this.richTextBoxPad.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxPad.Name = "richTextBoxPad";
            this.richTextBoxPad.Size = new System.Drawing.Size(326, 275);
            this.richTextBoxPad.TabIndex = 0;
            this.richTextBoxPad.Text = "";
            this.richTextBoxPad.TextColor = PadUserControls.RtfColor.Black;
            this.richTextBoxPad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBoxCopyPast_KeyDown);
            this.richTextBoxPad.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.richTextBoxCopyPast_MouseWheel);
            this.richTextBoxPad.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBoxPad_LinkClicked);
            this.richTextBoxPad.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBoxCopyPast_KeyUp);
            // 
            // notifyIconCopyPast
            // 
            this.notifyIconCopyPast.ContextMenuStrip = this.contextMenuStripTray;
            this.notifyIconCopyPast.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconCopyPast.Icon")));
            this.notifyIconCopyPast.Text = "Copy|Past";
            this.notifyIconCopyPast.Visible = true;
            this.notifyIconCopyPast.DoubleClick += new System.EventHandler(this.notifyIconCopyPast_DoubleClick);
            // 
            // contextMenuStripTray
            // 
            this.contextMenuStripTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAutoExec,
            this.toolStripSeparator2,
            this.toolStripMenuItemShow,
            this.toolStripSeparator1,
            this.toolStripMenuItemExit});
            this.contextMenuStripTray.Name = "contextMenuStripTray";
            this.contextMenuStripTray.Size = new System.Drawing.Size(125, 82);
            // 
            // toolStripMenuItemAutoExec
            // 
            this.toolStripMenuItemAutoExec.CheckOnClick = true;
            this.toolStripMenuItemAutoExec.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAutoExec.Image")));
            this.toolStripMenuItemAutoExec.Name = "toolStripMenuItemAutoExec";
            this.toolStripMenuItemAutoExec.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemAutoExec.Text = "Autorun";
            this.toolStripMenuItemAutoExec.Click += new System.EventHandler(this.toolStripMenuItemAutoExec_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripMenuItemShow
            // 
            this.toolStripMenuItemShow.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemShow.Image")));
            this.toolStripMenuItemShow.Name = "toolStripMenuItemShow";
            this.toolStripMenuItemShow.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemShow.Text = "Show";
            this.toolStripMenuItemShow.Click += new System.EventHandler(this.toolStripMenuItemShow_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemExit.Image")));
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // timerInternetConnectionCheck
            // 
            this.timerInternetConnectionCheck.Interval = 15000;
            this.timerInternetConnectionCheck.Tick += new System.EventHandler(this.timerInternetConnectionCheck_Tick);
            // 
            // backgroundWorkerConnectionCheck
            // 
            this.backgroundWorkerConnectionCheck.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerConnectionCheck_DoWork);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 275);
            this.Controls.Add(this.richTextBoxPad);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Opacity = 0.1;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "_Pad";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyUp);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.contextMenuStripTray.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private RichTextBoxPad richTextBoxPad;
        private System.Windows.Forms.NotifyIcon notifyIconCopyPast;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTray;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemShow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAutoExec;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Timer timerInternetConnectionCheck;
        private System.ComponentModel.BackgroundWorker backgroundWorkerConnectionCheck;
    }
}

