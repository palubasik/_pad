using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
namespace Constants
{
    static class VersionInfo
    {
        public const string APP_NAME = "_Pad";
        public const string APP_VER = "1.0.2";
        public const string APP_NAME_VER = "_Pad 1.0.2";
    }

    static class ZoomFactor {
        public const float ZoomDelta = 0.1F;
        public const float ZoomMax = 10F;
        public const float ZoomMin = 0.3F;
    }

    static class Opacity {
        public const double OpacityMax = 1F;
        public const double OpacityMin = 0.1F;
        public const double OpacityDelta = 0.05F;
    }

    static class AutoExecConstants{
        public static string SystemPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Startup)+@"\";//@"c:\Documents and Settings\All Users\Start Menu\Programs\Startup\";
        public const string AppName = VersionInfo.APP_NAME + ".lnk";
    };
    static class ExternalFiles{
        public static string MainWindowParameters = String.Format("{0}\\{1}", Application.StartupPath, "WinParam.bin");
    };
    static class FormSize{
        public const int SizeMin = 10;
        public const int SizeMax = 600;
        public const int SizeDelta = 50;
    };
}
