﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pad
{
    public struct BindCommand
    {
        public string Key;
        public string Command;
    }
}
