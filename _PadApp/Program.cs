using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Pad
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
         static ISoundEngine engine = null;
        static ISound sound = null;
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}