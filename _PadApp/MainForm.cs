using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using IWshRuntimeLibrary;
using System.Reflection;
using ConfigParamsSerializer;
using PadUserControls;
using System.Net;
using Global;

namespace Pad
{
    public partial class MainForm : Form
    {
        [Serializable]
        class CPWindowState{
            public Point location; 
            public Size size;
            public double formOpacity;
            public string PadData = String.Empty;
        };

        Size lastSize = new Size(100,100);

        private bool bCtrlPressed = false;
        private bool bAltPressed = false;
       
        
        private List<BindCommand> bindList = new List<BindCommand>();

        public List<BindCommand> BindList
        {
            get { return bindList; }
            set { bindList = value; }
        }

        PadConsole console = null;

        public MainForm()
        {
          
            InitializeComponent();
            //_PadControls.TextArea = this.richTextBoxPad;
            //_PadControls.ScreenForm = this;
        //   RichTextBoxPad pad = new RichTextBoxPad();
       //     pad.ScreenForm = this;
        //    pad.TextArea = this.richTextBoxPad;
            console = new PadConsole(this.richTextBoxPad);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItemShow_Click(object sender, EventArgs e)
        {
            Show();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
                bCtrlPressed = true;
            else if (e.Alt)
                bAltPressed = true;
           
        }

        private void richTextBoxCopyPast_KeyDown(object sender, KeyEventArgs e)
        {
            MainForm_KeyDown(sender, e);  
        }

        private void richTextBoxCopyPast_KeyUp(object sender, KeyEventArgs e)
        {
            MainForm_KeyUp(sender, e);
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (bCtrlPressed)
            {
                bCtrlPressed = false;
                string str_key = e.KeyData.ToString().Split(',')[0];

                foreach (BindCommand bk in bindList)
                {
                    if (String.Compare(bk.Key, str_key, true) == 0)
                    {
                        console.Parse(bk.Command);
                        return;
                    }
                }
            }
            
            if (bAltPressed)
                bAltPressed = false;
         
            
            
            if (e.KeyData == Keys.Enter) 
            {
            //    int command_row = richTextBoxPad.GetLineFromCharIndex(richTextBoxPad.SelectionStart) - 1;
            //    if(command_row >=0)
              //  int last_ind = richTextBoxPad.Text.LastIndexOf(">>");
                int idx = -1;
                int ln = 0;
                for (int i = richTextBoxPad.SelectionStart - 2; i > 0; i--)
                {
                    if (++ln > 255)
                        break;
                    if (richTextBoxPad.Text[i] == '\n')
                        break;
                    if (richTextBoxPad.Text[i] == '>' && richTextBoxPad.Text[i - 1] == '>')
                    {
                        idx = i;
                        break;
                    }
                }
                if (idx > 0)
                {
                     
                    string data = richTextBoxPad.Text.Substring(idx + 1,richTextBoxPad.SelectionStart - idx - 2);

                    console.Parse(data);
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Hide();
                e.Cancel = true;
            }
            else 
            {
                SaveStartWindowParameters();
            }
        }

        private void richTextBoxCopyPast_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
           
            if (bCtrlPressed)
            {
                if (e.Delta > 0)
                {
                    if (richTextBoxPad.ZoomFactor < Constants.ZoomFactor.ZoomMax)
                        richTextBoxPad.ZoomFactor += Constants.ZoomFactor.ZoomDelta;
                }
                else
                {
                    if (richTextBoxPad.ZoomFactor > Constants.ZoomFactor.ZoomMin)
                        richTextBoxPad.ZoomFactor -= Constants.ZoomFactor.ZoomDelta;
                }
               
            }else if(bAltPressed){

                if (e.Delta > 0)
                {
                    if (Opacity < Constants.Opacity.OpacityMax)
                        Opacity += Constants.Opacity.OpacityDelta;
                }
                else
                {
                    if (Opacity > Constants.Opacity.OpacityMin)
                        Opacity -= Constants.Opacity.OpacityDelta;
                } 

            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            notifyIconCopyPast.Text = Constants.VersionInfo.APP_NAME_VER;
            toolStripMenuItemAutoExec.Checked = System.IO.File.Exists(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
            LoadStartWindowParameters();
            richTextBoxPad.CarretToEnd();
            this.Opacity = Constants.Opacity.OpacityMax;
            timerInternetConnectionCheck.Enabled = true;
        }
        
        private void SaveStartWindowParameters(){
            try
            {
                CPWindowState obj = new CPWindowState();
                obj.location = this.Location;
                obj.size = this.Size;
                obj.formOpacity = this.Opacity;
                obj.PadData = richTextBoxPad.Text;
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(Constants.ExternalFiles.MainWindowParameters, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, obj);
                stream.Close();
            }catch{}
        }
        
        private void LoadStartWindowParameters()
        {
            try
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(Constants.ExternalFiles.MainWindowParameters, FileMode.Open, FileAccess.Read, FileShare.Read);
                CPWindowState obj = (CPWindowState)formatter.Deserialize(stream);
                stream.Close();
                int max_w = SystemInformation.PrimaryMonitorSize.Width - this.Size.Width;
                int max_h = SystemInformation.PrimaryMonitorSize.Height - this.Size.Height;
                this.Left = obj.location.X < max_w ? obj.location.X : max_w;
                this.Top = obj.location.Y < max_h ? obj.location.Y : max_h;
                
                this.Height = obj.size.Height < max_h ? obj.size.Height : max_h;
                this.Width = obj.size.Width < max_w ? obj.size.Width : max_w;
                
                this.Opacity = obj.formOpacity;
                richTextBoxPad.Text = obj.PadData;
            }
            catch { }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveStartWindowParameters();
        }

        private void toolStripMenuItemAutoExec_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName))
            {
                System.IO.File.Delete(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
            }

            if (toolStripMenuItemAutoExec.Checked == false)
            {
                return;
            }

            try
            {
                WshShellClass wshShell = new WshShellClass();

                IWshShortcut MyShortcut;

                MyShortcut = (IWshRuntimeLibrary.IWshShortcut)wshShell.CreateShortcut(Constants.AutoExecConstants.SystemPath + Constants.AutoExecConstants.AppName);
                MyShortcut.TargetPath = Application.ExecutablePath;
                MyShortcut.Description = Constants.VersionInfo.APP_NAME_VER;
                MyShortcut.IconLocation = Application.StartupPath + @"\app.ico";
                MyShortcut.Save();

            }
            catch
            {
                toolStripMenuItemAutoExec.Checked = false;
            }
        }

   

        private void notifyIconCopyPast_DoubleClick(object sender, EventArgs e)
        {

            Show();
            this.WindowState = FormWindowState.Normal;

        }
        
        private void MainForm_Resize(object sender, EventArgs e)
        {
            if(WindowState == FormWindowState.Minimized){
                //lastSize = this.Size;
                Hide();
                //WindowState = FormWindowState.Normal;
            }
        }

        private void richTextBoxPad_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(e.LinkText);
            }
            catch { }
        }


        private void timerInternetConnectionCheck_Tick(object sender, EventArgs e)
        {
            if(!backgroundWorkerConnectionCheck.IsBusy)
                backgroundWorkerConnectionCheck.RunWorkerAsync();
        }
        
        #region Internet Connection Check
        int GlobalFailedConnectionCount = 0;
        private void backgroundWorkerConnectionCheck_DoWork(object sender, DoWorkEventArgs e)
        {
            HttpWebRequest req;
            HttpWebResponse resp;
            try
            {
                req = (HttpWebRequest)WebRequest.Create("http://www.google.com");
                resp = (HttpWebResponse)req.GetResponse();

                if (resp.StatusCode.ToString().Equals("OK"))
                {
                    if (GlobalData.isInternetAvailable == false) 
                    {
                        notifyIconCopyPast.BalloonTipIcon = ToolTipIcon.Info;
                        notifyIconCopyPast.BalloonTipText = "Internet now available";
                        notifyIconCopyPast.ShowBalloonTip(1000);
                        GlobalData.isInternetAvailable = true;
                    }
                }
                else
                {
                    GlobalFailedConnectionCount++;
                    if (GlobalFailedConnectionCount > 2 && GlobalData.isInternetAvailable == true)
                    {
                        notifyIconCopyPast.BalloonTipText = "Internet connection failed";
                        notifyIconCopyPast.ShowBalloonTip(10);
                        GlobalData.isInternetAvailable = false;
                        GlobalFailedConnectionCount = 0;
                    }
                    
                }
            }
            catch (Exception)
            {
                GlobalFailedConnectionCount++;
                if (GlobalFailedConnectionCount > 2 && GlobalData.isInternetAvailable == true)
                {
                    notifyIconCopyPast.BalloonTipText = "Internet connection failed";
                    notifyIconCopyPast.ShowBalloonTip(10);
                    GlobalData.isInternetAvailable = false;
                    GlobalFailedConnectionCount = 0;
                }
               
            }
        }
        #endregion
    }
}