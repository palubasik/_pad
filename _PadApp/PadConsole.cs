﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Reflection;
using Global;
using PadUserControls;

namespace Pad
{
    class PadCommand 
    {
        public string CommandClassName = String.Empty;
        public string CommandClassMethod = String.Empty;
        public string Name = String.Empty;
        public string Library = String.Empty;
        public int ParamCount = 0;
    }

    public class PadConsole
    {
        Dictionary<String, Assembly> loadedAssemblies = new Dictionary<String, Assembly>();
        Dictionary<String, object> commandClassesInstances = new Dictionary<string, object>();
        string padCommandsFile = String.Empty;
        RichTextBoxPad padControls = null;

        
        public string PadCommandsFile
        {
            get { return padCommandsFile; }
            set { padCommandsFile = value; }
        }
         List<PadCommand> commands = new List<PadCommand>();

         public PadConsole(RichTextBoxPad pad)
         {
             padControls = pad;
        
            Assembly asm = Assembly.GetCallingAssembly();
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly a in assemblies)
            {
                try
                {
                    loadedAssemblies.Add(a.Location, a);
                }
                catch { }
            }

            if (ConfigurationSettings.AppSettings["PadCommandsFile"] != null)
                padCommandsFile = Application.StartupPath + ConfigurationSettings.AppSettings["PadCommandsFile"];
            else
                padCommandsFile = Application.StartupPath + "\\Data\\PadCommands.txt";
            
            LoadCommands();

        }
        public  string LastCommand = String.Empty;
      

        public  bool Parse(string text) 
        {

            LastCommand = ">>" + text;
            string[] parse_data = ConsoleEx.ParseConsoleLine(text);
            if (parse_data == null)
            {
                padControls.InsertText("Invalid paramerers format. Use correct framing by \"\n");
                return false;
            }

            PadCommand pc = commands.Find( s => String.Compare(s.Name,parse_data[0],true) == 0);
            if (pc == null)
            {
                padControls.InsertText( "Command not found or invalid command format\n");
                return false;
            }


            ExecutePadCommand(parse_data, pc);
            
            return true;
        }
         void plug_CallException(Exception e)
        {
            padControls.InsertText("\n\tException thrown duting command execution:\n");
            padControls.InsertText(String.Format("\tMessage:{0}\nStack:{1}\n",e.Message,e.StackTrace));
        }



         private void ExecutePadCommand(string[] command_data, PadCommand pc)
         {
             string command_class_key = String.Format("{0}:{1}", pc.Library, pc.CommandClassName);
             Type commandClassType = null;
             ICommandPlugin plug = null;
             if (!commandClassesInstances.ContainsKey(command_class_key))
             {



                 commandClassType = GetPadCommandClassType(pc);
                 try
                 {
                     plug = (ICommandPlugin)Activator.CreateInstance(commandClassType);
                     if (commandClassType == null || plug == null)
                     {

                         throw new Exception();
                     }
                 }
                 catch
                 {
                     padControls.InsertText(String.Format("Invalid command configuration. Check pad command config.\n"));
                     return;

                 }
                 //plug.CallException += new ErrorHandler(plug_CallException);
                 plug.CallPadException += new Global.ExceptionHandler(plug_CallException);
                 plug.Init(this.padControls);
                 commandClassesInstances.Add(command_class_key, plug);
             }
             else
             {
                 commandClassType = commandClassesInstances[command_class_key].GetType();
                 plug = (ICommandPlugin)commandClassesInstances[command_class_key];
             }

             try
             {

                 MethodInfo methodInfo = commandClassType.GetMethod(pc.CommandClassMethod);

                 int param_count = (command_data.Length - 1);
                 object[] paramS = null;
                 if (pc.ParamCount > 0)
                 {
                     paramS = new object[pc.ParamCount];
                     for (int i = 0; i < pc.ParamCount; i++)
                     {
                         if (i > param_count - 1)
                         {

                             break;
                         }
                         paramS[i] = command_data[i + 1];
                     }

                     if (pc.ParamCount < param_count)
                     {
                         string last = String.Empty;
                         for (int i = pc.ParamCount - 1; i < param_count; i++)
                             last += String.Format(" {0}", command_data[i + 1]);
                         last = last.TrimStart();
                         paramS[pc.ParamCount - 1] = last;
                     }
                 }

                 methodInfo.Invoke(plug, paramS);
             }
             catch (Exception)
             {
                 padControls.InsertText("Error during command execution.Invalid command format.\n");
             }
         }

        private  Type GetPadCommandClassType(PadCommand pc)
        {
            Assembly asm = GetLibrary(pc);

            //string pref_namespace = String.Empty;
            //if (!pc.CommandClassName.Contains("!"))
            //    pref_namespace = "_Pad.";
            //else pc.CommandClassName = pc.CommandClassName.Replace("!", ".");


            return asm.GetType(pc.CommandClassName);
        }

        private  Assembly GetLibrary(PadCommand pc)
        {
            Assembly asm = Assembly.GetCallingAssembly();
            if (String.Compare(pc.Library, String.Format("{0}\\Lib\\this", Application.StartupPath), true) != 0)
            {
              
                if (loadedAssemblies.ContainsKey(pc.Library))
                {
                    asm = loadedAssemblies[pc.Library];
                }
                else
                {
                    asm = Assembly.LoadFrom(pc.Library);
                    loadedAssemblies[pc.Library] = asm;
                }
            }
            return asm;
        }

        private  void LoadCommands()
        {
            commands.Clear();

            try
            {    
                using (StreamReader sr = new StreamReader(padCommandsFile))
                {
                    while (true)
                    {
                        string s = sr.ReadLine();
                        if (s == null)
                            break;
                        if (s.Split('|').Length < 4)
                            continue;
                        if (s.StartsWith("--"))
                            continue;
                        string[] data = s.Split('|');
                        PadCommand xpc = new PadCommand();
                        xpc.Name = data[0].Trim();
                        string[] class_data = data[1].Trim().Split('.');
                       
                        if(class_data.Length < 2)
                        {
                            MessageBox.Show(String.Format("Error data for command: {0}", data[2].Trim()), "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            throw null;
                        }
                        xpc.CommandClassName = data[1].Trim().Substring(0, data[1].LastIndexOf('.'));
                        xpc.CommandClassMethod = class_data[class_data.Length - 1];

                        xpc.Library = data[2].Trim();
                        if (!xpc.Library.Contains(":\\"))
                            xpc.Library = String.Format("{0}\\Lib\\{1}", Application.StartupPath, xpc.Library);

                        int param_count;
                        try {
                            param_count = int.Parse(data[3].Trim());
                        }
                        catch
                        {
                            MessageBox.Show(String.Format("Error data for command: {0}", data[0].Trim()), "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            throw null;
                        }
                        xpc.ParamCount = param_count; 
                        commands.Add(xpc);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error loading _Pad command file. Check configuration.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                Application.Exit();
            }
        }

    }
}
