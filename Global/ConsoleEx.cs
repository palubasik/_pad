﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Global
{
    public class ConsoleEx
    {
        public static string[] ParseConsoleLine(string text)
        {
            string[] parse_data = null;
            if (!text.Contains("\""))
                parse_data = text.Trim().Split(' ');
            else
            {
                List<string> parse = new List<string>();
                string work = text;
                bool bOpen = false;
                while (true) 
                {
                    bOpen = !bOpen;
                    
                    int idx = work.IndexOf('\"');
                    if (idx == -1)
                        if (bOpen)
                            break;
                        else
                            return null;//invalid format
                    string cutted = work.Substring(0, idx).Trim();
                    if (!String.IsNullOrEmpty(cutted))
                    {
                        if(bOpen)
                            parse.AddRange(cutted.Split(' '));
                        else
                            parse.Add(cutted);

                    }
                    work = work.Substring(idx + 1);
                }
                parse_data = parse.ToArray();
            }
            return parse_data;
        }
        
        public static bool CompareWildcard(string WildString, string Mask, bool IgnoreCase)
        {
            int i = 0, k = 0;

            while (k != WildString.Length)
            {
                if (Mask.Length -1 < i)
                    return false;
                switch (Mask[i])
                {
                    case '*':

                        if ((i + 1) == Mask.Length)
                            return true;

                        while (k != WildString.Length)
                        {
                            if (CompareWildcard(WildString.Substring(k + 1), Mask.Substring(i + 1), IgnoreCase))
                                return true;

                            k += 1;
                        }

                        return false;

                    case '?':

                        break;

                    default:

                        if (IgnoreCase == false && WildString[k] != Mask[i])
                            return false;
                        if (IgnoreCase && Char.ToLower(WildString[k]) != Char.ToLower(Mask[i]))
                            return false;

                        break;
                }

                i += 1;
                k += 1;
            }

            if (k == WildString.Length)
            {
                if (i == Mask.Length || Mask[i] == ';' || Mask[i] == '*')
                    return true;
            }

            return false;
        }
    }
}
