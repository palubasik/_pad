﻿using System;
using System.Collections.Generic;
using System.Text;
using PadUserControls;

namespace Global
{
    public delegate void ExceptionHandler(Exception ex);

    public interface ICommandPlugin
    {
        event ExceptionHandler CallPadException;
        void Init(PadUserControls.RichTextBoxPad pad);
    }
    
}
