﻿using System;
using System.Collections.Generic;
using System.Text;
using PadUserControls;

namespace Global
{
    public abstract class BasePadCommand :ICommandPlugin
    {
        public event Global.ExceptionHandler CallPadException;

        protected RichTextBoxPad padControl = null;
        
        public void Init(RichTextBoxPad pad) {
                padControl = pad;
        }
        
        protected void SendException(Exception e) 
        {
            CallPadException(e);
        }

        protected bool PadAssertStringNullOrEmpty(string input, string errorMsg) 
        {
            if (String.IsNullOrEmpty(input)) 
            {
                padControl.InsertText(String.Format("{0}\n",errorMsg));
                return false;
            }
            return true;
        }
    }
}
