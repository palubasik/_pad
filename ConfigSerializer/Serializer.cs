﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace ConfigParamsSerializer
{
    public class Serializer
    {
        static void SerializeToConfig(Assembly asm) 
        {
            Type[] types = asm.GetTypes();
            foreach (Type t in types) 
            { 
                PropertyInfo[] pil = t.GetProperties();
                foreach (PropertyInfo pi in pil)
                {
                    if (pi.GetCustomAttributes(typeof(PadSerializedItem), true).Length > 0)
                    {
                        //pi.Name;
                        //pi.GetValue(null, null);

                    }
                }
            }
        }
    }

    public class SerializerException : Exception 
    { 
        public SerializerException()
            : base()
        { }
        public SerializerException(string text)
            : base(text)
        { }
        public SerializerException(string text, Exception exc)
            : base(text, exc)
        { }

    }
}
