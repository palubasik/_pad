﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing;
using Global;

namespace Commands
{
    class CommonCommands : BasePadCommand
    {

        public CommonCommands() 
        {
           // GlobalData.CurrentDirectory = Environment.GlobalData.CurrentDirectory;
        }
      
        public void GetCurrentDirectory() 
        {
             padControl.InsertText(String.Format("{0}\n",GlobalData.CurrentDirectory));
        }
         
        public void Dir() 
        {
             padControl.InsertText(String.Format(" {0}\n\n", GlobalData.CurrentDirectory));
            foreach (string dir in Directory.GetDirectories(GlobalData.CurrentDirectory)) 
            {
                 padControl.InsertText( String.Format("\t[{0}]\n", GetShortName(dir)));
            }
            foreach (string file in Directory.GetFiles(GlobalData.CurrentDirectory)) 
            {
                 padControl.InsertText(String.Format("\t{0}\n",GetShortName( file)));
            }
        }

        public  void ChDir(string dir) 
        {
            if (String.IsNullOrEmpty(dir))
            {
                 padControl.InsertText(String.Format("{0}\n",GlobalData.CurrentDirectory));
                return;
            }

            int ind = dir.LastIndexOf(":\\");
            if (ind != -1)
            {
                if (!Directory.Exists(dir))
                {
                     padControl.InsertText(String.Format("The system cannot find the path specified.\n"));
                    return;
                }
                else
                {
                    GlobalData.CurrentDirectory = dir;
                }
            }
            else 
            {
                if (dir.Contains(".."))
                {
                    
                        GlobalData.CurrentDirectory = GlobalData.CurrentDirectory.Substring(0, GlobalData.CurrentDirectory.LastIndexOf("\\"));
                        if (GlobalData.CurrentDirectory.Length < 3) {
                            GlobalData.CurrentDirectory += "\\";
                        }
                }
                else
                {
                    string next_dir = String.Format("{0}\\{1}",GlobalData.CurrentDirectory,dir);
                    if (!Directory.Exists(next_dir))
                    {
                         padControl.InsertText(String.Format("The system cannot find the path specified.\n"));
                        return;
                    }
                    else
                    {
                        GlobalData.CurrentDirectory = next_dir;
                      
                    }
                }
            }
             padControl.InsertText(String.Format("  {0}\n", GlobalData.CurrentDirectory));
        }

        public bool LoadFile(string path)
        {
            bool fOk = false;

            if (String.IsNullOrEmpty(path))
            {
                //_Pad.TextArea.InsertTextAsRtf("Specify file name.\n");
                OpenFileDialog od = new OpenFileDialog();
                od.InitialDirectory = GlobalData.CurrentDirectory;
                if (od.ShowDialog() == DialogResult.OK)
                {
                    path = od.FileName;
                }
                else
                    return fOk;
            }

            string load_str = null;

            if (path.Contains(":\\"))
                load_str = path;
            else
                load_str = String.Format("{0}\\{1}", GlobalData.CurrentDirectory, path);

            try
            {
                padControl.LoadFile(load_str);
                fOk = true;
            }
            catch
            {
                try
                {

                    padControl.LoadFile(load_str, RichTextBoxStreamType.PlainText);
                    fOk = true;
                }
                catch
                {
                     padControl.InsertText(String.Format("Error loading file {0}\n", path));
                }
            }
            padControl.CarretToEnd();
            return fOk;
        }

        public bool SaveToFile(string path)
        {
            bool fOk = false;

            if (String.IsNullOrEmpty(path))
            {
                //_Pad.TextArea.InsertTextAsRtf("Specify file name.\n");
                SaveFileDialog od = new SaveFileDialog();
                od.InitialDirectory = GlobalData.CurrentDirectory;
                
                if (od.ShowDialog() == DialogResult.OK)
                {
                    path = od.FileName;
                }
                else
                    return fOk;
            }

            string save_str = null;

            if (path.Contains(":\\"))
                save_str = path;
            else
                save_str = String.Format("{0}\\{1}", GlobalData.CurrentDirectory, path);

            try
            {
                padControl.RemoveLastCommand();
                //padControl.Text = padControl.Text.Replace(_PadConsole.LastCommand + "\n", String.Empty);
                padControl.SaveFile(save_str);
                 padControl.InsertText("Saved!");
                fOk = true;
            }
            catch
            {
                 padControl.InsertText(String.Format("Error saving to file {0}\n", path));
            }
            return fOk;
        }

        public  void GetDate()
        {
             padControl.InsertText(String.Format("{0}\n", DateTime.Now.ToString()));
        }

        public void ClearScreen()
        {
            padControl.Clear();
        }

        public void ChangeForm(string command)
        {
            if (String.IsNullOrEmpty(command) || !command.Contains("=") || command.Split('=').Length < 2)
            {
                 padControl.InsertText("Invalid command format: use: '>>form field=value:value_type (default: string)'\n");
                return;
            }
            ChangeControlProperty(padControl.Parent, command);
        }

        public void ChangeTextArea(string command)
        {
            if (String.IsNullOrEmpty(command) || !command.Contains("=") || command.Split('=').Length < 2)
            {
                 padControl.InsertText("Invalid command format: use: '>>textarea field=value:value_type (default: string)'\n");
                return;
            }
            ChangeControlProperty(padControl, command);
        }

        public  void Exit() 
        {
            padControl.RemoveLastCommand();
            Application.Exit();
            // padControl.Parent.GetType().GetMethod("Exit").Invoke(padControl.Parent, new object[] { }); 
        }

        public void LoadImage(string path)
        {

            if (String.IsNullOrEmpty(path))
            {
                //_Pad.TextArea.InsertTextAsRtf("Specify file name.\n");
                OpenFileDialog od = new OpenFileDialog();
                od.InitialDirectory = GlobalData.CurrentDirectory;
                if (od.ShowDialog() == DialogResult.OK)
                {
                    path = od.FileName;
                }
                else
                    return;
            }

            string load_str = null;

            if (path.Contains(":\\"))
                load_str = path;
            else
                load_str = String.Format("{0}\\{1}", GlobalData.CurrentDirectory, path);

            try
            {
                Image im = Image.FromFile(load_str);
                padControl.InsertImage(im);
            }
            catch
            {
                 padControl.InsertText(String.Format("Error loading file {0}\n", path));
            }
            padControl.CarretToEnd();
        }
        
        private  void ChangeControlProperty(Control ctrl, string command)
        {
            string[] data = command.Split('=');
            Type padType = ctrl.GetType();
            PropertyInfo field = padType.GetProperty(data[0]);//, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            if (field == null)
            {
                 padControl.InsertText(String.Format("Property '{0}' was not found.\n", data[0]));
                return;
            }

            string[] value_data = data[1].Split(':');
            try
            {
                if (value_data.Length == 1)
                    field.SetValue(ctrl, data[1], null);
                else
                {
                    Type value_type = Type.GetType(value_data[1]);
                    if (value_type == null)
                    {
                         padControl.InsertText(String.Format("Unrecognized type: {0} .\n", value_data[1]));
                        return;
                    }
                    field.SetValue(ctrl, Convert.ChangeType(value_data[0], value_type), null);
                }
            }
            catch
            {
                 padControl.InsertText("Error during set value. Check property type.\n");
            }
        }

        private  string GetShortName(string file)
        {
            int ind = file.LastIndexOf("\\");
            if (ind < 0)
                return file;
            return file.Substring(ind + 1);
        }
        
    }
}
