﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Global;

namespace Commands
{
    public class ReportCommands : BasePadCommand
    {
        
      

         string reportFileName = String.Format("Report_{0}_{1}.txt",DateTime.Now.Month,DateTime.Now.Year); 
        
        public void ReportText(string text) 
        {

            if (String.IsNullOrEmpty(text)) 
            {
                 padControl.InsertText("Specify report text\n");
                return;
            }
            using (StreamWriter sw = new StreamWriter(reportFileName,true)) 
            {
                sw.WriteLine(String.Format("{0}\t{1}", DateTime.Now.ToString("M"), text));
                sw.Flush();
                sw.Close();
            }

             padControl.InsertText("Added.\n");
        }

        public void DeleteReport() 
        {
            if (File.Exists(reportFileName)) {
                File.Delete(reportFileName);
            }
             padControl.InsertText("Done.\n");
        }

        public  void ShowReport() 
        {
            using (StreamReader sr = new StreamReader(reportFileName, true))
            {
                 padControl.InsertText(sr.ReadToEnd());
                sr.Close();
            }
        }
    }
}
