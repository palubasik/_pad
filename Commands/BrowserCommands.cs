﻿using System;
using System.Collections.Generic;
using System.Text;
using Global;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace Commands
{
    public class BrowserCommands : BasePadCommand 
    {
      
        public void Exec(string file_name) 
        {
            if (String.IsNullOrEmpty(file_name))
            {
                //padControl.ScreenForm)
                 padControl.InsertText("Command format error. Type: >>!|exec file_name\n");
                return;
            }
            if (!file_name.Contains(":\\"))
                file_name = String.Format("{0}\\{1}",Application.StartupPath,file_name);
            try 
            {
                System.Diagnostics.Process.Start(file_name); 
            }
            catch 
            {
                 padControl.InsertText("Error during execution.\n");
            }
        }
        
        //public void Req(string sUrl) 
        //{ 
        //    if(!PadAssertStringNullOrEmpty(sUrl,"Command format error. Type: >>req url\n"))
        //        return;
        //    if (!sUrl.Contains("http://") && !sUrl.Contains("https://"))
        //        sUrl = "http://" + sUrl;

        //    try
        //    {
        //        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sUrl);
        //        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
        //        Encoding enc = System.Text.Encoding.GetEncoding(1251);

        //        using (StreamReader sr = new StreamReader(resp.GetResponseStream(),enc)) 
        //        {
                    
        //            string res = ConsoleEx.StripHTML(sr.ReadToEnd());
        //            if (res != null)
        //            {
        //                padControl.InsertText(res);
        //            }
        //            else
        //            {
        //                padControl.InsertText("Error during html parsing\n");
        //                return;
        //            }

        //        }
        //        resp.Close();
        //    }
        //    catch (Exception exc1)
        //    { 
        //        padControl.InsertText(String.Format("Error during execution: {0}\n",exc1.Message));
        //    }

        //}
       
        public void OpenLink(string sUrl)
        {

            if (String.IsNullOrEmpty(sUrl)) 
            {
                 padControl.InsertText("Command format error. Type: >>go|browse url\n");
                return;
            }
            try
            {
                if (!sUrl.Contains("http://") && !sUrl.Contains("https://"))
                    sUrl = "http://" + sUrl;
                System.Diagnostics.Process.Start(sUrl);

            }

            catch (Exception exc1)
            {

                // System.ComponentModel.Win32Exception is a known exception that occurs when Firefox is default browser.  

                // It actually opens the browser but STILL throws this exception so we can just ignore it.  If not this exception,

                // then attempt to open the URL in IE instead.

                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {

                    // sometimes throws exception so we have to just ignore

                    // this is a common .NET bug that no one online really has a great reason for so now we just need to try to open

                    // the URL using IE if we can.

                    try
                    {

                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", sUrl);
                        System.Diagnostics.Process.Start(startInfo);

                        startInfo = null;

                    }

                    catch (Exception)
                    {

                        // still nothing we can do so just show the error to the user here.
                         padControl.InsertText("Error during execution. Check web url.\n");
                    }

                }

            }

        }
    }
}
