﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using Global;
namespace Commands
{
    public class SystemInfo: BasePadCommand,
        ICommandPlugin
    {
        
      

   // Add each property of the SystemInformation class to the list box.
        public  void GetInfo(string text){ 
        Type t = typeof(System.Windows.Forms.SystemInformation);            
            PropertyInfo[] pi = t.GetProperties();
            List<string> info = new List<string>();
            bool bAll = text == null;
            for (int i = 0; i < pi.Length; i++)
            {
                if (!bAll)
                {
                    if (String.Compare(pi[i].Name, text, true) == 0)
                    {
                        info.Add(String.Format("\t{0} => {1}\n", pi[i].Name, pi[i].GetValue(null, null).ToString()));
                        break;
                    }
                    continue;
                }
                info.Add(String.Format("\t{0} => {1}\n", pi[i].Name, pi[i].GetValue(null, null).ToString()));
            }
            
            info.Sort();
            foreach (string s in info) 
            {
                 padControl.InsertText(String.Format("{0}", s));
            }
        }
    }
}
