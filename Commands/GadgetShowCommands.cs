﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Net;
using System.IO;
using System.Configuration;
using Global;

namespace Commands
{
    class GadgetShowCommands : BasePadCommand
    {
        private string wheatherImageUrl = String.Empty;

        public GadgetShowCommands() 
        {
            if (ConfigurationSettings.AppSettings["WeatherGadget"] != null)
            {
                wheatherImageUrl = ConfigurationSettings.AppSettings["WeatherGadget"];
            }
            else
                wheatherImageUrl = "http://informer.gismeteo.ru/new/4248-31.GIF";
        }

        public void ShowWheather() 
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(wheatherImageUrl);
                request.Timeout = 5000;
                 HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1251);
                Image im = Image.FromStream(response.GetResponseStream());
                padControl.InsertImage(im);
                padControl.InsertText("\n");
            }
            catch 
            {
                padControl.InsertText("Error showing wheather gadget.\n");
            }
        }
    }
}
