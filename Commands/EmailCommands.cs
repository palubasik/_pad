﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Net;
using System.IO;
using System.Configuration;
using Global;
using Email;

namespace Commands
{
    class EmailCommands : BasePadCommand
    {
       
        private string userid = "undefined";
        private string pop3server = String.Empty;
        int port = 0;
        private string password = String.Empty;
        public EmailCommands()
        {
            if (ConfigurationSettings.AppSettings["Email"] != null)
            {
                string []email_data = ConfigurationSettings.AppSettings["Email"].Split(';');
                userid = email_data[2];
                pop3server = email_data[0];
                port = int.Parse(email_data[1]);
                password = email_data[3];
            }
        }

        public void GetEmails(string count)
        {
            int email_count = 20;
            if (!String.IsNullOrEmpty(count)) 
            { 
                 int.TryParse(count, out email_count);
            } 

            try{
                Pop3MailClient mailClient = new Pop3MailClient(pop3server, port, true, userid, password);
                mailClient.Trace += new TraceHandler(Void);
                mailClient.ReadTimeout = 60000; //give pop server 60 seconds to answer
                int fact_number;
                int mailbox_size;
                mailClient.Connect();
                mailClient.GetMailboxStats(out fact_number, out mailbox_size);
                fact_number = fact_number > email_count ? email_count : fact_number;
                string text;
                PadAppendTextFormated(String.Empty);
                List<int> email_ids;
                mailClient.GetEmailIdList(out email_ids);

                
                for(int i = 0;i<fact_number;i++)
                {
                    mailClient.GetRawEmail(email_ids[email_ids.Count - i - 1], out text);
                    PadAppendTextFormated(text);
                    PadAppendTextFormated(String.Empty);

                }
                mailClient.UndeleteAllEmails();
                mailClient.Disconnect();
            }catch(Exception e)
            {
                padControl.InsertText(String.Format("Some error during email retriving:{0}",e.Message));
            }
        }

        public void GetEmailStat()
        {
            

            try
            {
                Pop3MailClient mailClient = new Pop3MailClient(pop3server, port, true, userid, password);
                mailClient.Trace += new TraceHandler(Void);
                mailClient.ReadTimeout = 60000; //give pop server 60 seconds to answer
                mailClient.Connect();
                int count = 0;
                int mailbox_size = 0;
                if(!mailClient.GetMailboxStats(out count, out mailbox_size))
                    PadAppendTextFormated("Email stat retrive error");
                else
                    PadAppendTextFormated(String.Format("Email count:{0} ; Maibox size:{1}",count,mailbox_size));
                mailClient.Disconnect();
            }
            catch (Exception e)
            {
                padControl.InsertText(String.Format("Some error during email retriving:{0}", e.Message));
            }
        }
        void Void(string text)
        { }
        void PadAppendTextFormated(string text) 
        {
            padControl.InsertText(String.Format("\t{0}\n", text));
        }
        
    }
}
