﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Global;

namespace Commands
{
    class WinCMD : BasePadCommand, IDisposable
    {
        Process cmdProcess = null;
        
        private delegate void TextDelegate(string text);

        public WinCMD()
        {

            ProcessStartInfo psi = new ProcessStartInfo("cmd.exe");
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardError = true;

            cmdProcess = Process.Start(psi);

            cmdProcess.OutputDataReceived += new DataReceivedEventHandler(cmdProcess_OutputDataReceived);
            cmdProcess.ErrorDataReceived += new DataReceivedEventHandler(cmdProcess_ErrorDataReceived);
        }
        
        public void Dispose() 
        {
            if(!cmdProcess.WaitForExit(1000))
                cmdProcess.Kill();
        }
     
        public void WriteCommand(string command)
        {
            if (cmdProcess == null) 
            { 
                SendException(new Exception("Cmd.exe process was not loaded."));
                return;
            }

            if (String.IsNullOrEmpty(command))
            {
                padControl.InsertText("Specify command\n");
                return;
            }
            try
            {
                cmdProcess.CancelOutputRead();
                cmdProcess.CancelErrorRead();
            }
            catch { }
            try
            {
              
               
                StreamWriter myStreamWriter = cmdProcess.StandardInput;
                
                myStreamWriter.WriteLine(command);
                
                cmdProcess.BeginErrorReadLine();
                cmdProcess.BeginOutputReadLine();
            }
            catch(Exception e) 
            {
                SendException(e);
            }
        }

        void cmdProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (padControl.InvokeRequired)
                {
                    TextDelegate callback = padControl.AppendText;
                    padControl.BeginInvoke(callback, new object[] { String.Format("\t{0}\n", e.Data) });

                }
                else
                {
                    padControl.InsertText(String.Format("\t{0}\n", e.Data));
                }
                //collectedOutData.Add(e.Data);
                //RaiseException(new Exception());
                //padControl.InsertTextAsRtf(String.Format("\t{0}\n", e.Data));
            }
        }
        
        void cmdProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (padControl.InvokeRequired)
                {
                    TextDelegate callback = padControl.AppendText;
                    padControl.BeginInvoke(callback, new object[] { String.Format("\t{0}\n", e.Data) });

                }
                else
                {
                    padControl.InsertText(String.Format("\t{0}\n", e.Data));
                }
                //collectedErrorData.Add(e.Data);
                //RaiseException(new Exception());

            }
        }

    }
}
