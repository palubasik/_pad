﻿using System;
using System.Collections.Generic;
using System.Text;
using Rss;
using Global;


namespace Commands
{
        class HtmlProcessor
    {
        private static string[,] arr2HtmlSymbolsToChange = new string[,] { 
                                            { "<br>", "  " }, 
                                            { "&quot;", "\"" }, 
                                            { "&amp;", "&" },
                                            { "&lt;", "<" },
                                            { "&gt;", ">" },
                                            { "&nbsp;", " " } };

        public static string ReplaceHtmlSymbols(string inp)
        {
            string str = inp;
            for (int i = 0; i < arr2HtmlSymbolsToChange.Length / 2; i++)
            {
                str = str.Replace(arr2HtmlSymbolsToChange[i, 0], arr2HtmlSymbolsToChange[i, 1]);
            }
            return str;
        }
    }

         class RssCommands : BasePadCommand
        {
            
          

         Dictionary<string, List<string>> cached_data = new Dictionary<string, List<string>>();

        public  void GetRss(string sUrl, string max_items, string no_link)
        {
            if (String.IsNullOrEmpty(sUrl))
            {
                 padControl.InsertText("Command format error. Type: >>rss url [max_items=20] [no_link=false]\n");
                return;
            }
            List<RssChannel> channels = new List<RssChannel>();
            StringBuilder output = new StringBuilder();

            if (!sUrl.Contains("http://"))
                sUrl = "http://" + sUrl;

            //bool random_show = false;
            int max = 20;
            bool nolink = false;
            if (!int.TryParse(max_items, out max))
                max = 20;
            if (!bool.TryParse(no_link, out nolink))
                nolink = false;
          //  bool.TryParse(random, out random_show);
            try
            {
                RssFeed DaFeed = RssFeed.Read(sUrl);
                RssChannel DaChannel = (RssChannel)DaFeed.Channels[0];
                channels.Add(DaChannel);
                output.AppendFormat("\n{0}\n ", DaChannel.Title);

                int ms = 0;

                foreach (RssItem sTrm in DaChannel.Items)
                {
                    if (ms < max)
                    {
                        output.Append(String.Format("[{0}] {1}\n{2}\n{3}\n\n", sTrm.PubDate, sTrm.Title, 
                            HtmlProcessor.ReplaceHtmlSymbols(sTrm.Description), nolink == false ? sTrm.Link.ToString() : String.Empty));
                    }
                    else break;
                    ms++;
                }
                 padControl.InsertText(output.ToString());
            }
            catch 
            {
                 padControl.InsertText("Some error during command execution. Check connection or URL.");
            }
        }

    }
}
