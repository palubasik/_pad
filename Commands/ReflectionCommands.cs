﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Global;

namespace Commands
{
    public class ReflectionCommands : BasePadCommand
    {
        

        private  string TryToGetValue(PropertyInfo pi)
        {
            string res = "Error on getting value.";
            try
            {
                res = pi.GetValue(null, null).ToString();
            }
            catch { }
            return res;
        }

        public  void GetProperties(string type_name, string command)
        {

            if (String.IsNullOrEmpty(type_name))
            {
                 padControl.InsertText("Type: >> rf_prop type_name [-n:name (property name)] [-sv (show value))]\n");
                return;
            }
            string property_name = String.Empty;
            bool show_value = false;
            if (command != null)
            {
                string[] splitted = command.Split(' ');
                for (int i = 0; i < splitted.Length; i++)
                {
                    switch (splitted[i])
                    {
                        case "-n":
                            if (++i < splitted.Length)
                            {


                                property_name = splitted[i];
                            }
                            else
                            {
                                 padControl.InsertText("Invalid property name\n");
                                return;
                            }
                            break;
                        case "-sv":
                            show_value = true;
                            break;

                    }
                }
            }
            Type t = Assembly.GetCallingAssembly().GetType(type_name);
            if (t == null)
            {
                 padControl.InsertText("Type not found.\n");
                return;
            }
            if (String.IsNullOrEmpty(property_name))
            {
                PropertyInfo[] pi = t.GetProperties();
                if (pi == null)
                {
                     padControl.InsertText("Error during getting properties.\n");
                    return;
                }
                for (int i = 0; i < pi.Length; i++)
                     padControl.InsertText(String.Format("{0} {1}\n", pi[i].Name, show_value == true ? String.Format("=> {0}", TryToGetValue(pi[i])) : String.Empty));
            }
            else
            {
                PropertyInfo pi = t.GetProperty(property_name);
                if (pi == null)
                {
                     padControl.InsertText(String.Format("Error during getting property {0}.\n",property_name));
                    return;
                }
                 padControl.InsertText(String.Format("{0} {1}\n", pi.Name, show_value == true ? String.Format("=> {0}", TryToGetValue(pi)) : String.Empty));
            }

        }
    }
}
