﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Global;
using System.Configuration;
using System.Windows.Forms;
using System.Drawing;
using PadUserControls;

namespace Commands
{
    public struct CommandInfo 
    {
        public string CommandName;
        public string CommandDesc;
        public string Category;
    }

    public class HelpCommands : BasePadCommand
    {


        List<CommandInfo> command_help_list = new List<CommandInfo>();
        string padCommandsFile;

        public HelpCommands() : base() 
        {
            if (ConfigurationSettings.AppSettings["PadCommandsFile"] != null)
                padCommandsFile = Application.StartupPath + ConfigurationSettings.AppSettings["PadCommandsFile"];
            else
                padCommandsFile = Application.StartupPath + "\\Data\\PadCommands.txt";
            using (StreamReader sr = new StreamReader(padCommandsFile))
            {
                while (true)
                {
                    string s = sr.ReadLine();
                    if (s == null)
                        break;
                    if (s.StartsWith("--"))
                        continue;
                    string[] spl = s.Split('|');

                    if (spl.Length < 6) 
                    {
                        padControl.InsertText(String.Format("Warning! Invalid data in command file: {0}", s));
                        continue;
                    }

                    CommandInfo ci = new CommandInfo();

                    ci.CommandName = spl[0].Trim();
                    ci.CommandDesc = spl[4].Trim();
                    ci.Category = spl[5].Trim();
                    command_help_list.Add(ci);
                }
                sr.Close();
            }
            command_help_list.Sort(new CommandInfoComparer());
        }

        public void Help(string command) 
        {
            
            bool all = command == null;
            List<CommandInfo> ci_list = null;
            if(all)
                ci_list = command_help_list;
            else
                ci_list = command_help_list.FindAll(x => ConsoleEx.CompareWildcard(x.CommandName,command,true)); 
            string last = null;
            
            foreach (CommandInfo ci in ci_list)
            {
                if (last != ci.Category)
                {
                    padControl.InsertText("\n");
                    //hard-coded bold:)
                    padControl.InsertText("  ");
                    padControl.InsertTextAsRtf(@"{\rtf1\ansi\ansicpg1252\deff0{\fonttbl{\f0\fnil\fcharset204 Microsoft Sans Serif;}{\f1\fnil\fcharset0 ;}}{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1\pard\lang1049\b\f0\fs17"
                        + String.Format("{0} commands", ci.Category) + @"\lang1033\f1\fs20\par}");
                  last = ci.Category;
                }
                padControl.InsertText(String.Format("\t{0} - {1}\n", ci.CommandName, ci.CommandDesc));
            }
            
        }

        public class CommandInfoComparer : IComparer<CommandInfo>
        {
            public int Compare(CommandInfo ci, CommandInfo ci2)
            {
                int compare = String.Compare(ci.Category, ci2.Category, true);
                if (compare == 0) 
                {
                    compare = String.Compare(ci.CommandName, ci2.CommandName);
                }
                return compare;
            }
        } 
    }
}
